### Learning Activities Application.


### Dev env setup with no docker. Requires `ruby 3.1.2`, `bundler` and `sqlite3`

All dependencies are expected to be available in the system. To install that, [a brief guide from GoRails](https://gorails.com/setup) can be used.

The steps to run an app locally after clonning:
```
bundle

rails db:crete db:migrate

rails db:seed

rails s
```

### Description of the API endpoints with cURL examples

#### GET /api/v1/products

```
curl localhost:3000/api/v1/products
```
