# frozen_string_literal: true

FactoryBot.define do
  factory :product do
    code { 'MyString' }
    name { 'MyString' }
    price { '9.99' }
  end
end
