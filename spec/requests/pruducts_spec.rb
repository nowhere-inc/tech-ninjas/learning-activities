# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Products' do
  before { create(:product) }

  it 'list products' do
    get('/api/v1/products')

    expect(response).to have_http_status(:success)
  end
end
