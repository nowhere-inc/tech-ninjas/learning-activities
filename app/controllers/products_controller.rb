# frozen_string_literal: true

class ProductsController < ApplicationController
  def show
    render json: Product.all.as_json(only: %i[code name price])
  end
end
